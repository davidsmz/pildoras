import java.util.Scanner;

public class ClaseEnum {
    // La tipos enumerados se declara fuera del metodo main
    // enum TALLA {MINI, MEDIANO, GRANDE, MUY_GRANDE}

    // La clase de tipo enumerado acepta constructores
    // y metodos getter y setter
    enum TALLA {

        MINI("S"), MEDIANO("M"), GRNADE("L"), MUY_GRANDE("XL");

        // Los tipos de clase enumerada no admite la creacion
        // de objetos
        private String abreviatura;

        private TALLA(String abreviatura) {
            this.abreviatura = abreviatura;
        }

        public String getAbreviatura() {
            return abreviatura;
        }
    }

    public static void main(String[] args) {

        /*
        Objeto Enum, donde se puede almacenar unos tipos
        valores y solo se puede almacenar ese tipo de valores
        y no acepta otros tipos mas
         */

//        TALLA s = TALLA.MINI;
//        TALLA m = TALLA.MEDIANO;
//        TALLA l = TALLA.GRANDE;

        Scanner sc = new Scanner(System.in);
        System.out.printf("Escribe una talla, MINI, MEDIANO, GRANDE O EXTRAGRANDE\n");
        String datos = sc.next().toUpperCase();

        TALLA talla = Enum.valueOf(TALLA.class, datos);

        System.out.printf("La talla ingresada es %s\n", talla.getAbreviatura());

    }
}
