package poo;

// Con extends se hereda propiedades y metodos de una Superclase
// La clase Furgoneta hereda de Coche
// La herencia multiple no se admite en Java (para esto hay interfaces)


public class Furgoneta extends Coche {

    private int capacidadCarga;
    private int plazasExtra;

    public Furgoneta(int capacidadCarga, int plazasExtra) {
        // Llamar al constructor de la clase padre con super
        // Iniciamos las propiedades del contructor del padre
        super();
        this.capacidadCarga = capacidadCarga;
        this.plazasExtra = plazasExtra;
    }

    public int getCapacidadCarga() {
        return capacidadCarga;
    }

    public int getPlazasExtra() {
        return plazasExtra;
    }
}
