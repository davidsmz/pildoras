// Un paquete es como una carpeta en la cual organiza clases
package poo;

// Modularizar es separar en clases para poder leer mejor codigo

public class Coche {

    // encapsular con el modificador de acceso private
    // para midificar propiedades solo desde la misma clase
    private int ruedas;
    private int largo;
    private int ancho;
    private int motor;
    private int pesoBase;
    private String color;
    private int pesoCapote;
    private int pesoTotal;
    private boolean asientosCuero;
    private boolean climatizador;
    private int precio;

    // Metodo constructo en el cual seda estado inicial a las propiedades
    // Este metodo tiene que tener el mismo nombre que la clase
    public Coche() {
        ruedas = 4;
        largo = 400; // en cm
        ancho = 200; // en cm
        motor = 1600; // en cm cubicos
        pesoBase = 500; // en Kg
    }

    /*
    Metodo getter and setter
    - getter o captador:
      proporcionar el valor de la propiedad
      * function: devolver el valor de las porpiedades de los objetos
      * syntax: public int nameMethod() { coding + return }
    - setter o definidor:
      modificamos el valor de la propiedad
      * function: modificar el valor de las propiedades de los objetos
      * syntax: public void nameMethod() { coding }
    */

    // geetter ruedas
    public int getRuedas() {
        return ruedas;
    }

    // getter  ancho
    public int getAncho() {
        return ancho;
    }

    // getter  largo
    public int getLargo() {
        return largo;
    }

    // getter motor
    public int getMotor() {
        return motor;
    }

    // getter pesoBase
    public int getPeso() {
        return pesoBase;
    }

    // getter and setter de color

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        // El valor this hace referencia a la propia clase
        this.color = color;
    }

    // getter and setter de asientosCuero
    public String getAsientosCuero() {
        if (asientosCuero){
            return "si";
        }else {
            return "no";
        }
    }

    public void setAsientosCuero(String asientosCuero) {
        if (asientosCuero.equalsIgnoreCase("si")){
            this.asientosCuero = true;
        }else {
            this.asientosCuero = false;
        }
    }

    // getter and setter de climatizador


    public String getClimatizador() {
        if (climatizador){
            return "si";
        }else {
            return "no";
        }
    }

    public void setClimatizador(String climatizador) {
        if (climatizador.equalsIgnoreCase("si")){
            this.climatizador = true;
        }else {
            this.climatizador = false;
        }
    }

    // getter and setter pesoCapote
    public int getPesoCapote() {
        return pesoCapote;
    }

    public void setPesoCapote(int pesoCapote){
        this.pesoCapote = pesoCapote;
    }

    // getter pesoTotal
    public int getPesoTotal() {
        pesoTotal = pesoBase + pesoCapote;
        if (asientosCuero) {
            pesoTotal = pesoTotal + 20;
        }

        if (climatizador) {
            pesoTotal = pesoTotal + 10;
        }
        return pesoTotal;
    }

    // getter precio
    public int getPrecio() {
        if (asientosCuero) {
            precio += 3000;
        }

        if (climatizador) {
            precio += 4000;
        }

        return precio;
    }

}

