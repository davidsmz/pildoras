package poo;

// Empleado aqui esta llamando a su constructor
// La palabra finnal hace que la clase ya no tenga clases heredadas
public class Jefe extends Empleado implements Jefes {

    private double incentivo;

    public Jefe(String nombre, double sueldo, int year, int month, int day){

        // Se llama al constructor de la clase padre
        // Se llama al constructor que corresponda dependiendo
        // Del numero y tipo de parametro correspondiente a un constructor

        super(nombre, sueldo, year, month, day);
    }

    public void setIncentivo(double incentivo) {
        this.incentivo = incentivo;
    }

    public double getIncentivo() {
        return incentivo;
    }

    public String tomarDesicion(String desicion) {
        return "El jefe ha tomado la desicion de " + desicion;
    }

    public double estableceBonus(double gratificacion) {
        double prima = 3000;
        return Trabajadores.bonusBase + gratificacion + prima;
    }

    // En esta clase el metodo getSueldo() reemplaza al mismo de la clase padre
    public double getSueldo(){
        // Para invocar al metodo de la clase padre se utiliza super
        double sueldo = super.getSueldo();
        double sueldoJefe = sueldo + incentivo;
        return sueldoJefe;
    }
}
