package poo;

import poo.Persona;

public class Alumno extends Persona {

    private String carrera;

    public Alumno(String nombre, String carrera){
        super(nombre);
        this.carrera = carrera;
    }

    public String getCarrera() {
        return carrera;
    }

    // metodo de la clase abstracta
    public String getDescription() {
        return "El alumno" + getNombre() + " estuia " + getCarrera();
    }
}
