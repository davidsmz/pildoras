package poo;

public class PersonaUso {
    public static void main(String[] args){

        // Debido a que la clase persona es Abstracta
        // por el metodo getDescripcion() que es diferente
        // para la clase empleado y la clase alumno
        // la respuesta de este metodo sera distinto para cada
        // posicion del array en la muestra de consola

        Persona[] personas = new Persona[2];
        personas[0] = new Empleado("David", 2000, 2017, 12, 16);
        personas[1] = new Alumno("Caleb", "Ingenieria Electronica");

        for (Persona e : personas) {
            System.out.printf("Nombre: %s - Descripcion: %s\n",e.getNombre(), e.getDescription());
        }

    }
}
