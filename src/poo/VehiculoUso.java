package poo;

public class VehiculoUso {

    public static void main(String[] args){

        Coche miCoche1 = new Coche();
        miCoche1.setColor("Azul");

        Furgoneta miFurgoneta1 = new Furgoneta(200, 6);

        // Por herencia el objeto de la clase Furgoneta tambien tendra
        // metodos de la clase Coche como por ejemplo el metodo setColor

        miFurgoneta1.setColor("Negro");
        miFurgoneta1.setAsientosCuero("si");
        miFurgoneta1.setClimatizador("si");

        System.out.printf("El color del coche es %s \n", miCoche1.getColor());
        System.out.printf("El color de la furgoneta es %s \n", miFurgoneta1.getColor());
        System.out.printf("La capacidad de carga de la furgoneta es %d kg\n", miFurgoneta1.getCapacidadCarga());
        System.out.printf("El precio de mi Furgoneta es %d\n", miFurgoneta1.getPrecio());

    }
}
