package poo;

import javax.swing.*;

public class CocheUso {
    public static void main(String[] args) {

        // Instanciar clase Coche, Ejemplar de la clase Coche
        // ferrari es un objeto perteneciente a la clase Coche
        Coche ferrari = new Coche();

        // no se puede modificar la propiedad ruedas de la clase coche
        // ferrari.ruedas = 3;
        // porque esta encapsulada con el modficador de acceso private
        // System.out.printf("El coche ferrari tiene %d ruedas\n", ferrari.ruedas);
        // Por esto tampoco se puede usar esta propiedad directamente
        // Para acceder a las propiedades lo podemos hacer desde un metodo
        // No se puede manipular directamente la propiedad
        // Estos motodos donde nos comunicamos se llaman geter and setter

        // accedemos a la propiedad ruedas con el metodo getter
        System.out.printf("El coche ferrari tiene %d ruedas\n", ferrari.getRuedas());

        // Nueva instancia de la clase
        Coche miCoche = new Coche();

        // modificar el color del coche, o establecer su color
        // para esto se utiliza el metodo setter
        // El metodo setter utiliza el paso de parametros
        // miCoche.setColor("color")
        miCoche.setColor(JOptionPane.showInputDialog("Ingrese el color del coche"));


        // acceder al color del coche
        // para esto se utiliza el metodo getter
        System.out.printf("El color de mi coche es %s\n", miCoche.getColor());

        // miCoche.setAsientosCuero("si")
        miCoche.setAsientosCuero(JOptionPane.showInputDialog("¿ El coche tiene Asientos de cuero ?"));
        System.out.printf("Mi coche %s tiene asientos de cuero\n", miCoche.getAsientosCuero());

        miCoche.setPesoCapote(300);

        miCoche.setClimatizador(JOptionPane.showInputDialog("¿ El coche tiene Climatizador ?"));
        System.out.printf("Mi coche %s tiene climatizador\n", miCoche.getClimatizador());

        System.out.printf("El peso total del coche es %d kg\n", miCoche.getPesoTotal());

        System.out.printf("El precio del coche es $%d\n", miCoche.getPrecio());
    }
}
