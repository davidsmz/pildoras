package poo;
// En un fichero pueden ir varias clases
// Solo la clase que tiene el nombre del fichero es public

/** Las clases final indican que ya no pueden heredar
 * otras clases de la misma
 */

/** Los metodos final indican que no pueden haber otros
 * metodos con el mismo nombre en las subclases, es decir
 * no se pueden reescribir el metodo en una subclase heredada
 * de la superclase donde se encuentra el metodo final
 */

import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;

public class EmpleadoUso {
    public static void main(String[] args) {

        /*
        Polimorfismo, se puede utilizar un objeto de la subclase, siempre
        que el programa espere un objeto de la superclase.
        Es decir un objeto de la superclase puede recibir un objeto de la subclase.
        Un objeto puede comportarse de dediferente forma dependiendo del
        conexto, Las variables objeto son polimorficas.
        */

        Jefe jefeMarketing = new Jefe("Neill", 4000, 2005, 1, 1);
        jefeMarketing.setIncentivo(2000);

        EmpleadoUso obj = new EmpleadoUso();

        Empleado empleado1 = new Empleado("David", 2000, 2010, 9, 6);
        Empleado empleado2 = new Empleado("Caleb", 3000, 2015, 7, 3);
        Empleado empleado3 = new Empleado("Danie", 5000, 2017, 8, 4);
        /** Se utiliza el metodo constructor que tiene como parametro
         * el nombre y establece los parametros por defectoinidcados en este
         */
        Empleado empleado4 = new Empleado("Raquel");
        // Ejemplo de polimorfismo
        // Principio de sustitucion
        Empleado empleado5 = jefeMarketing;

        Empleado[] empleados = new Empleado[6];
        empleados[0] = empleado1;
        empleados[1] = empleado2;
        empleados[2] = empleado3;
        empleados[3] = empleado4;
        empleados[4] = empleado5;

        // Otro ejemplo de polimorfismo
        // Dentro del objeto Empleado esta entrando un objto Jefe
        // En este caso el empleado[4] se comportaria como objeto empleado
        empleados[5] = new Jefe("Maria", 7000, 2008, 7, 17);
        Jefe jefeFinanzas = (Jefe) empleados[5];
        jefeFinanzas.setIncentivo(3000);

        // Vemos como no se puede hacer cast de Empleado a Jefe
        // Esto se debe a la herencias
        // Ya que un Jefe siempre es un empleado en tanto que viceversa no
        // Jefe jefeCompras = (Jefe)empleados[1];

        System.out.println(jefeFinanzas.tomarDesicion("Dar mas vacaciones a empleados"));
        System.out.printf("El jefe %s tiene bonus de %f", jefeFinanzas.getNombre(),
                jefeFinanzas.estableceBonus(500));

        // Imprimir Datos de empleados
        obj.motrarDatos(empleados);

        // Aumentar el sueldo de empleados en un porcentaje
        // El objeto JefeMarketing se comporta com objeto de clase Empleado
        // Entonces aumenta el sueldo pero como objeto empleado
        // Entonces no aumento el sueldo de la clase Jefe solo Empleado
        obj.subirSueldo(empleados, 10);

        // Para utilizar estemetodo se implementa clase abstracta
        Arrays.sort(empleados);
        // Imprimir Datos de empleados
        obj.motrarDatos(empleados);

         /*
        La Maquita virtual de Java es capaz de detectar a que metodo
        tiene que llamar, como vemos en las clases Jefe y Empleado hay
        metodos getSueldo() , entonces la maquina virtual de Java puede
        realizar ese reconociemiento y eso se llama Enlazado Dinamico,
        Es saber en tiempo de ejecucion a que metodo va a llamar
         */

    }

    void motrarDatos(Empleado[] empleados) {
        for (int i = 0; i < empleados.length; i++) {
            System.out.printf("\n");
            System.out.printf("id: %d Nombre: %s - Sueldo: %.2f - fecha: %s",
                    empleados[i].getId(),
                    empleados[i].getNombre(),
                    empleados[i].getSueldo(),
                    empleados[i].getFechaContratacion());
        }
        System.out.printf("\n");
    }

    void subirSueldo(Empleado[] empleados, int porcentaje) {
        // El bucle foreach o for mejorado
        for (Empleado e : empleados) {
            e.setSubirSueldo(porcentaje);
        }
    }
}


/** La interfaz se utiliza para heredae metodos de varias clases
 * las interfaces solo tienen metodos  abstractos y contantes
 * se utiliza con la palabra clave implements en clase
 * las interfaces no  se pueden instanciar
 */
class Empleado extends Persona implements Comparable, Trabajadores {

    // El constructor por defecto es aquel que sus estados iniciales son 0 o null
    public Empleado(String nombre, double sueldo, int year, int month, int day) {
        // El metodo constructor invoca al super de la clase padre
        super(nombre);
//        this.nombre = nombre;
        this.sueldo = sueldo;
        // month va de 0 a 11
        GregorianCalendar fecha = new GregorianCalendar(year, month-1, day);
        this.fechaContratacion = fecha.getTime();
        this.id = idSiguiente;
        idSiguiente++;
    }

    // Sobrecarga de Contructores, varios constructores en una misma clase
    // No puede haber otro constructor con el mismo numero de parametros
    // y el mismo orden de los tipos de parametros
    // Contructor con un estado inicial diferente

    public Empleado(String nombre){
        // empleo del this en el contructor
        // para establecer propipedades por defecto
        //this.nombre = nombre;
        this(nombre, 1000, 2017, 1, 1);
    }

    // getter nombre
    // Este metodo es heredado de la clase Persona
//    public String getNombre() {
//        return nombre;
//    }

    // getter sueldo
    public double getSueldo() {
        return sueldo;
    }

    // getter fechaContratacion
    public Date getFechaContratacion() {
        return fechaContratacion;
    }

    // setter
    public void setSubirSueldo(double porcentaje){
        sueldo += (sueldo * porcentaje)/100;
    }

    // getter
    /**
     * Los metodos static son inherentes a la clase y no al objeto
     * creado de la clase por eso se pueden llamar invocando primero
     * a la clase y no al objeto, además los metodos static no acceden
     * a los campos de ejemplar (variables declaradas en la clase)  a no ser
     * que estas sean tambien estaticas
     */
    public int getId() {
        return id;
    }

    // metodo de la clase abstracta
    public String getDescription() {
        return "El empleado con id: " + id + " tiene un sueldo de S/" + sueldo;
    }

    // Implementacion del metodo abstracto compareTo de la clase Comparable
    public int compareTo(Object myObject) {
        Empleado otroEmpleado = (Empleado) myObject;
        if (this.id < otroEmpleado.id) {
            return -1;
        }
        if (this.id > otroEmpleado.id) {
            return 1;
        }

        return 0;
    }

    public double estableceBonus(double gratificacion) {
        return Trabajadores.bonusBase + gratificacion;
    }

    // Las propiedades pueden ir al final o inicio de la clase
    private String nombre;
    private double sueldo;
    private Date fechaContratacion;
    private int id;
    /**
     * Las propiedades static son inherentes a la clase
     * y no al objeto por eso todos los objetos creados de una clase
     * comparten la misma propiedad static es decir no se crea una copia
     * en la memoria de esta propiedad, y al ser llamados tienen que ser
     * desde la misma clase y no de el objeto.
     */
    private static int idSiguiente = 1;
}
