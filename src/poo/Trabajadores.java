package poo;

public interface Trabajadores {

    // las constantes en las interfaces son public static final
    double bonusBase = 1500;

    double estableceBonus(double gratificacion);
}
