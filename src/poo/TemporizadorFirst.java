package poo;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

public class TemporizadorFirst {
    public static void main(String[] args) {
        /** La clase Timer como constructor recibe 2 parametros
         * el primero es el delay y el segundo es una interfaz
         * que se ha tenido que implementar esta es ActionListener
         */
        DameHora oyente = new DameHora();

        Timer miTemporizador = new Timer(5000, oyente);
        miTemporizador.start();

        JOptionPane.showMessageDialog(null, "Pulse aceptar para detener");

        System.exit(0);
    }
}

class DameHora implements ActionListener {
    @Override
    // se implementa el metodo actionPerformed de la interfaz ActionListener
    public void actionPerformed(ActionEvent e) {
        Date ahora = new Date();
        System.out.println("Te pongo la hora cada 5 seg: " + ahora);
    }
}