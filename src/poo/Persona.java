package poo;

// Clase abstracta es aquella que tiene un metodo abstracto

public abstract class Persona {

    private String nombre;

    public Persona(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    // Metodo abstracto
    // Este metodo solo se declara y se utiliza
    // en las demas clases herededad
    // Esto es debido a que este metodo es distinto
    // para cada clase heredada
    public abstract String getDescription();
}
